# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kget
pkgver=23.08.1
pkgrel=1
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/internet/org.kde.kget"
pkgdesc="A versatile and user-friendly download manager"
license="GPL-2.0-or-later AND LGPL-2.0-only AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	gpgme-dev
	kcmutils5-dev
	kcompletion5-dev
	kconfig5-dev
	kconfigwidgets5-dev
	kcoreaddons5-dev
	kcrash5-dev
	kdbusaddons5-dev
	kdelibs4support-dev
	kdoctools5-dev
	ki18n5-dev
	kiconthemes5-dev
	kio5-dev
	kitemmodels5-dev
	kitemviews5-dev
	knotifications5-dev
	knotifyconfig5-dev
	kparts5-dev
	kservice5-dev
	ktextwidgets5-dev
	kwallet5-dev
	kwidgetsaddons5-dev
	kwindowsystem5-dev
	kxmlgui5-dev
	libktorrent-dev
	libmms-dev
	plasma-workspace-dev
	qca-dev
	qt5-qtbase-dev
	samurai
	sqlite-dev
	"
_repo_url="https://invent.kde.org/network/kget.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kget-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
f8620622149d742ad7fb6539085ecc2754af7c8b4172244b36b9b5aeafe6fea1b0b5ed56ad750b808b55e32a8bf4a287627f0ccc40c1548416eac54a728b3bcb  kget-23.08.1.tar.xz
"
